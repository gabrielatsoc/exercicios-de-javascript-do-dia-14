const gods = require('./gods');

let organizedGods = [];
let pantheons = [];

//pega todos os pantheons e armazena na variavel pantheon
gods.forEach(god => pantheons.includes(god.pantheon)?"":pantheons.push(god.pantheon));

//organiza os pantheons por ordem alfabética.
pantheons.sort(function(a, b){
    if(a<b) { return -1; }
    if(a>b) { return 1; }
    return 0;
});

//organiza os deuses de acordo com o pantheon, buscando um tipo de pantheon por vez e jogando esse deus desse pantheon pra variavel organizedGods, terminando ent com uma lista com todos os gods organizados pelo pantheon e esses pela ordem alfabética.
pantheons.forEach(pant => gods.forEach(god => god.pantheon === pant?organizedGods.push(god):""));

console.log(organizedGods);